/* 
 * File:   container.h
 * Author: debian
 *
 * Created on December 18, 2014, 4:19 PM
 */

#ifndef CONTAINER_H
#define	CONTAINER_H

// serialization support
#include <boost/serialization/serialization.hpp>


// worker support
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>

// levenstein 
#include "levenshtein-distance.hpp"

// thread pool support
#include "threadpool.hpp"


// for internal storage
#include <vector>

// for testing
#include <chrono>
#include <thread>
#include "tools/cpu-time.hpp"


// this value defines the speeding of search and remove operations:

#define NUMTHREADS 1


namespace trees {
    
    
     namespace bktree_detail {
        
     
         // helpers
        boost::mutex m_io_monitor;

        void print(std::string text) {
            boost::mutex::scoped_lock lock(m_io_monitor);
            std::cout << text;
        } 
        
             boost::mutex mutex_remove;  
             
              template<
            typename DataType,
            typename KeyType,
            typename MetricType
        >        
        class worker {
            
            public: 
                typedef std::vector<std::pair<KeyType, MetricType> >  WorkerReturnType; 
             
            public: 
            
                worker(DataType * _dt,KeyType _key, MetricType _mt,bool _op, bool _use_first_result): dt(_dt),
                        key(_key),mt(_mt), op(_op), use_first_result(_use_first_result)
                {}
                ~worker() {}
                
                

                void run() {    
// for testing purpose
                    latency = tools::get_wall_time()-latency;

			if (found) {
				ready = true; 
				std::cout<<"Found!\n";
				return;
			}
                    std::cout<<"Starting a job on ..."<<key<<"\n";
                    try {
// bk-tree
                        result = dt->find_within(key, mt,  op, use_first_result);
			  if (result.size()>0) found = true;
//std::vector<std::string>
/*
                        levenshtein_distance_operator_v2 ld;                                                    
                        for (auto it = dt->begin(); it!=dt->end(); it++  ) {
                        	//std::cout<<*it<<"\n";
                            MetricType distance = ld(*it,key);
                            //std::cout<<"distance ="<<distance<<"\n";
                            
                          if (distance <= mt)  {
                              std::cout<<"found \n";
				found = true;	
                              result.push_back(std::pair<KeyType,MetricType>(key,distance));break;
                          }
                        }
*/
                        
                    } catch (...) {  fail = true; }
                    
                    ready = true;    
                        
                    std::cout<<"Finishing a job on ..."<<key<<" with found "<<result.size()<<" connected entries"<<"\n";
                }
                
                // makes a remove of the entry
                void remove() {
                    try {
                            
                            boost::unique_lock< boost::mutex > lock( trees::bktree_detail::mutex_remove );                            
//                            std::cout<<"!!!Starting a job on ..."<<key<<"\n";
                            dt->remove(key);
//                            std::cout << "!!!finished the lock..."<<key<<"\n";
                            lock.unlock();
                        
                    } catch (...) { }
                                            
                    ready = true;  
                    remover = true;
                }
                
        
                WorkerReturnType getResult()  { return result;}
                KeyType getKey()  { return key;}
                
                bool isFail () const { return fail;}
                bool isReady () const { return ready;}
                bool isRemover () const { return remover;}
                
                
                
         
         
            private:
                DataType * dt;
                KeyType  key;
                MetricType  mt;
                bool op;
                bool use_first_result;
                WorkerReturnType result;
                bool ready = false;
                bool fail = false;
                bool remover = false;
            public:
                double latency = tools::get_wall_time();
		static bool found;
                                                
               

        };
       

        
        }    // end of bktree_detail namespace
     
template<
            typename DataType,
            typename KeyType,
            typename MetricType
        >        
bool trees::bktree_detail::worker<DataType, KeyType, MetricType>::found = false;  
     
        
        /* container class definition */
     
     template <
            typename DataType,
            typename KeyType,
            typename MetricType
	> 
        class container {

            public:
                typedef boost::shared_ptr<trees::bktree_detail::worker<DataType,KeyType,MetricType> > workerPtr;
                typedef typename trees::bktree_detail::worker<DataType,KeyType,MetricType>::WorkerReturnType WorkerReturnType;
                typedef  std::multimap<KeyType,WorkerReturnType>  ContainerResultType;
            
                
                container(unsigned int maxsize=50000):_maxsize(maxsize),_cursize(0) {
                
                    DataType * data = new DataType();
                    _storage.push_back(data);
                    _tp_ptr_vec.push_back(new boost::threadpool::pool(NUMTHREADS));


                }
                ~container() {
                
                    std::for_each(_storage.begin(), _storage.end(), 
                        [](DataType * &p) { 
                        if (p != nullptr) delete p;    
                        });                
                
                    std::for_each(_tp_ptr_vec.begin(), _tp_ptr_vec.end(), 
                        [](boost::threadpool::pool * &p) { 
                        if (p != nullptr) delete p;    
                        });                                                        
                
                }
                
                
                
                void insert(const KeyType &key) {
                
                                        
                    
                    if (_cursize>_maxsize) {
                        DataType * data = new DataType();                  
                        _storage.push_back(data);                         
                        _tp_ptr_vec.push_back(new boost::threadpool::pool(NUMTHREADS));
                        _cursize = 0;
                    }
//bk-tree
		       _storage.back()->insert(key);
                    

//std::vector<std::string>
//                    _storage.back()->push_back(key);
                    _cursize++;
                                                    
                }
                
                
                
                // performs an asynchronous search in the _storage.
                // signature corresponds to the trees::bktree::find_within method
                void find_within( KeyType key, MetricType d, bool op = true, bool use_first_result=false)  {
                    
			trees::bktree_detail::worker<DataType,KeyType,MetricType>::found = false;


                    for (unsigned int i =0; i< _storage.size();i++) {
                      
			std::cout<<"Submitting :::: "<<i<<"\n";
			if (trees::bktree_detail::worker<DataType,KeyType,MetricType>::found){
				
			std::cout<<"found and break :::: "<<i<<"\n";
				 break;
			}

                        workerPtr job (new trees::bktree_detail::worker<DataType,KeyType,MetricType>(_storage[i], key,d,op,use_first_result));    
                        _tp_ptr_vec[i]->schedule(boost::bind(&trees::bktree_detail::worker<DataType,KeyType,MetricType>::run, job));
                        _worker_results.push_back(job); //save the job and future results of its calculation 
                    }
                    
                    
                         std::for_each(_tp_ptr_vec.begin(), _tp_ptr_vec.end(), 
                        [](boost::threadpool::pool * &p) { 
                        if (p != nullptr)
                        if (!p->empty())    p->wait();
                        });   
                    
                }

		// returns the number of stored entries
		size_t size()  {
			size_t num_entries = 0;
			 std::for_each(_storage.begin(), _storage.end(),
			 [&num_entries] (DataType * &p) mutable {
				num_entries += p->size();
		         });	
			return num_entries;
		}
                // these two functions are used only for testing purpose: should be removed in the release version
                
                // could be run on a time-to-time basis: with a long period between calls
                void clean() {
                    
                    // clean the temporary job vector:
                    // if a job is finished (ready), save its result and remove the job from the vector
                    _worker_results.erase(                     
                                
                        std::remove_if(
                                _worker_results.begin(),
                                _worker_results.end(),
                                [this] ( workerPtr &p) mutable {
                                    
                                   /*
                                    std::cout<<"Cleaning a job\n";
                                    std::cout<<p->getKey()<<"\n";
                                    std::cout<<p->isReady()<<"\n";
                                    std::cout<<p->isFail()<<"\n";
                                    
                                            */ 
                                    
                                    if ( p->isReady() ) {
                                        if (!p->isRemover()) {
                                            latency_time.push_back(p->latency);
                                            this->_results.insert(std::pair<KeyType,WorkerReturnType>(p->getKey(),p->getResult()));
                                        }
                                        return true;
                                    }
                                    else if ( p->isFail() ) {                                      
                                      return true;
                                    } else return false;
                                    
                                }                    
                        ),
                         _worker_results.end()   
                         
                    );                    
                                        
                    
                }
                
                
                // the method might be called in the end to process the results
                ContainerResultType getResults() { return _results;}
                std::vector<double> getLatency() { return latency_time;}
                    
                
                
            private:
                
               std::vector<DataType *> _storage;
               unsigned int _maxsize;
               unsigned int _cursize; 
               std::vector<boost::threadpool::pool  *>  _tp_ptr_vec;
              // these containers are used only for testing purpose: should be removed in the release version
               std::vector<workerPtr> _worker_results;                
               ContainerResultType _results;                
               std::vector<double> latency_time; // to store the latency  of the tasks

                                    
                

     };
} // namespace trees

#endif	/* CONTAINER_H */

