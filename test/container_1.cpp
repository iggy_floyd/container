
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <map>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "container.h"
#include "bk-tree.hpp"


#include <chrono>
#include <thread>

using namespace std;







 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }




const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

//std::ifstream file("./BookerNames10K.txt");
//std::ifstream file("./BookerNames100K.txt");

// data to be processed: 3M entries
std::ifstream file("./BookerNames1M.txt");
//std::ifstream file2("./BookerNames1M.txt");
//std::ifstream file3("./BookerNames1M.txt");
//std::ifstream file2("./BookerNames10K.txt");
std::ifstream file3("./BookerNames10K.txt");
std::ifstream file4("./BookerNames10K.txt");
std::ifstream file5("./BookerNames10K.txt");
//std::ifstream file2("./BookerNames100K.txt");
//std::ifstream file3("./BookerNames100K.txt");


/*
 *  some parameters of this test
 * 
 *  max_pending_tasks -- defines how many tasks could be waiting internally in the container
 * 
 *  rate_of_exactness --  its inverse defines how oft the property should be found the container
 * 
 *  rate_of_cleaning  --  its inverse defines how oft the container cleans its internal storage 
 * 
 *  delay     --   in the real life,  there is a delay (it might be 10 ms or 1s or 10s  or ...) between "search calls"
 * 
 */
const int max_entries_tree = 50000;
const double rate_of_exactness = 0.005; // only one among 200 properties should be found in the container
const double rate_of_cleaning = 0.001; //  at each 1000th run of the container search, clean the internal storage
// assume that  we have a delay of 200 ms on average. Depends on different factors: insert time in the tree, communication with Cayley, 
// other code in the communicator.cpp,  rules (Double Booking, Overall) calculation, php background etc
size_t delay = 200; 


typedef trees::bktree<std::string, double, levenshtein_distance_operator_v2> datatype;
//typedef std::vector<std::string> datatype;
trees::container<datatype,std::string,double> bktreecontainer(max_entries_tree); 

bktreecontainer.insert(std::string(128,'#'));

// randomization
//bktreecontainer.randomization();
std::cout<<"randomization has been finished\n";

std::vector<std::string> tosearch;
std::vector<std::string> tosearch_orig;




double wallread=tools::get_wall_time();        
if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        
        char aa[sz+1];        
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    

        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));
        tosearch.push_back(std::string(bb)+line+"-"+std::string(aa));
        tosearch_orig.push_back(line);
        
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));
        tosearch.push_back(std::string(bb)+line+"-"+std::string(aa));
        tosearch_orig.push_back(line);

    }
      
    }
    
    file.close();
    std::cout<<"read has been finished\n";
}


/*

if (file2.is_open() && !file2.eof() ) {
    
    std::string line;     
    getline(file2, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
        
        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));
        
    }
    while (!file2.eof()) {
      getline(file2, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);   

        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));

    }
      
    }
    file2.close();
    std::cout<<"read has been finished\n";
} 

if (file3.is_open() && !file3.eof() ) {
    
    std::string line;     
    getline(file3, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
       bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));

        
    }
    while (!file3.eof()) {
      getline(file3, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));


    }
      
    }
    file3.close();
    std::cout<<"read has been finished\n";
}

if (file4.is_open() && !file4.eof() ) {
    
    std::string line;     
    getline(file4, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));

        
    }
    while (!file4.eof()) {
      getline(file4, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));


    }
      
    }
    file4.close();
    std::cout<<"read has been finished\n";
}
*/
/*
if (file5.is_open() && !file5.eof() ) {
    
    std::string line;     
    getline(file5, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
       bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));

        
    }
    while (!file5.eof()) {
      getline(file5, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktreecontainer.insert(std::string(bb)+line+"-"+std::string(aa));


    }
      
    }
    file5.close();
    std::cout<<"read has been finished\n";
}
*/

double wall01=0.,dt_wall01= 0.,wall02=0.;
int count=0,count_search=0;

size_t exactness = 1/rate_of_exactness;
size_t cleaning = 1/rate_of_cleaning;

for (unsigned int i =tosearch.size()-1 ;i>=tosearch.size()-20;i--) {
    count_search++;
    std::cout<<i<<"\n";
    std::string srch = (i%(exactness) == 0)?tosearch_orig[i]:tosearch[i];
    
    
    
    std::cout<<srch<<"\n";
    wall01=tools::get_wall_time();    
    bktreecontainer.find_within(srch,1.-0.80,false,true); // get with similarity of 0.80
    dt_wall01 += tools::get_wall_time() - wall01;
    
    // time-to-time cleaning of the internal storage
    if (i%(cleaning) == 0) bktreecontainer.clean();
    
    
    // a possible delay
    std::this_thread::sleep_for(std::chrono::milliseconds(delay)); // in ms 
    
}


// prepare the results
bktreecontainer.clean();

auto results = bktreecontainer.getResults();

for (auto& p : results) {
    if (p.second.size()>0) {
        count++;
                std::cout<<p.first<<" found to be connected to "<<p.second[0].first <<" with "<< p.second[0].second <<"\n";
    }
    else        std::cout<<p.first<<" is not connected " <<"\n";
            }


std::cout<<"\n\nBuffer size... "<< max_entries_tree<<" entries"<<"\n";
std::cout<<"\nAssumed delay between calls... "<<delay<<" ms\n";
std::cout<<"\nAll  properties stored... "<< bktreecontainer.size()<<"\n";
std::cout<<"\nTotal connected properties ... "<<count<<"\n";
std::cout<<"\nTotal time spent for "<< count_search <<" search operations  ... "<<dt_wall01<<" s\n";
std::cout<<"\nAverage time spent for search... "<< dt_wall01/count_search <<" s\n";

auto latency =bktreecontainer.getLatency(); 
std::cout<<"\nAverage Latency time from workers... "<< std::accumulate( latency.begin(), latency.end(), 0.0 )/ latency.size() <<" s\n";

    


return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(container_1)
{
using namespace boost::python;
def("test", test);

}



